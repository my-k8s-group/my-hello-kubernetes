#!/bin/bash

APP_NAME=my-hello-kubernetes
LOCAL_IP=$(hostname -I | cut -d " " -f1)
LOCAL_PORT=8080
REMOTE_PORT=8080
LISTEN_IP=0.0.0.0

if [[ $(which minikube) == "" ]]; then
  echo "Please install minikube and start again."
  exit
fi

if [[ $(minikube status | grep -i stop) != "" ]]; then
  echo "Starting minikube..."
  minikube start
fi

kubectl create -f ./application-deployment.yaml
kubectl create -f ./application-service.yaml

echo "Wait until service starts..."

sleep 15

echo "Use following ip-address to access a test stand with the \"$APP_NAME\" application from your local network:"
echo http://$LOCAL_IP:$LOCAL_PORT/

kubectl port-forward --address $LISTEN_IP service/my-hello-kubernetes $LOCAL_PORT:$REMOTE_PORT
